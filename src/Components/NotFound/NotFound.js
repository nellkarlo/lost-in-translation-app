import { useHistory } from 'react-router-dom'
import { useEffect, useState } from 'react'

// Component dealing with not found pages
const NotFound = () => {
    const [seconds, setSeconds] = useState(5)
    const history = useHistory()

    //timer until ridirecting user to homepage
    useEffect(() => {
        if (seconds > 0) {
            setTimeout(() => {
                setSeconds(seconds - 1)
            }, 1000)
        } else {
            return history.push('/')
        }
    }, [seconds, history])

    return (
        <>
            <h2>Page not found</h2>
            <p>
                Redirecting to startpage in {seconds} seconds...
            </p>
        </>
    )
}

export default NotFound
