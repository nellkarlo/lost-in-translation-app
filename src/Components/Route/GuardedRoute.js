import { Route, Redirect } from 'react-router-dom'
import { getLocalUser } from '../../Util/localStorage'

// Guarding the routes by checking if user is logged in.
// If true redirects to the desired component
// else redirects to startpage
const GuardedRoute = ({ component: Component }) => {
    return (

        <Route render={(props) => (

            getLocalUser()
                ? <Component {...props} />
                : <Redirect to='/' />
        )} />
    )
}

export default GuardedRoute