import "./Header.css"
import { clearLocalUser, getLocalUser } from "../../Util/localStorage"
import { useHistory, useLocation } from 'react-router-dom'
import { useState, useEffect } from 'react'

const Header = () => {
    const [logoutBtnVisible, setLogoutBtnVisible] = useState(false)
    const [profileBtnVisible, setProfileBtnVisible] = useState(false)
    const [username, setUsername] = useState('')

    const history = useHistory()
    const location = useLocation().pathname

    // Checking user logged in on location switch. shows and hides appropriate buttons. 
    useEffect(() => {
        const user = getLocalUser()
        if (user !== null) {
            setUsername(user)
        } else (setUsername(''))

        if ((location !== '/profile')) {
            setLogoutBtnVisible(false)
        } else { setLogoutBtnVisible(true) }

        if ((location !== '/translate')) {
            setProfileBtnVisible(false)
        } else { setProfileBtnVisible(true) }

    }, [location])

    // handle go to profile click
    const onGoToProfilePage = () => {
        return history.push('/profile')
    }
    // Handle logout scenario by clearing localstorage, username then pushing to startpage
    const onLogout = () => {
        clearLocalUser()
        setUsername('')
        return history.push('/')
    }

    return (
        <>
            <header id="header">
                <div id="titlecontainer">
                    <h1 className='p-2 mb-0'>
                        Lost in Translation
                    </h1>
                </div>
                <>
                    {!logoutBtnVisible && <p className='p-2 m-0'>Welcome {username}</p>}
                    <div className='d-flex flex-row-reverse'>
                        {profileBtnVisible && <button className='btn btn-secondary m-2' onClick={onGoToProfilePage}>Profile</button>}
                        {logoutBtnVisible && <button className='btn btn-danger m-2' onClick={onLogout}>Log out</button>}
                    </div>
                </>

            </header>
        </>

    )
}

export default Header

