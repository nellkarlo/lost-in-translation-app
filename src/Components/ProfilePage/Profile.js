import "./Profile.css"
import "react-confirm-alert/src/react-confirm-alert.css"
import { confirmAlert } from 'react-confirm-alert'
import signImageLinks from "../../Util/signImageLinks"
import shortid from 'shortid'
import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { Children } from 'react'
import { fetchUser, putUser } from "../../Util/Api"
import { somethingWentWrong, couldntUpdateUserAlert } from "../../Util/alerts"
import { clearLocalUser, getLocalUser } from "../../Util/localStorage"


const Profile = () => {
    const history = useHistory()

    const [signs, setSigns] = useState([])
    const [username, setUsername] = useState('')

    // Checks if user is logged in then retrieves all their translations with the
    // sign images. sets signs to retrieved imagelinks. 
    useEffect(() => {
        const loggedInUser = getLocalUser()

        fetchUser(loggedInUser).then(
            user => {

                setUsername(loggedInUser)

                const rawTexts = user[0].translations

                let signArray = rawTexts
                    .filter((translation) => translation.deleted === false)
                    .map((translation) => {

                        const imageLinks = signImageLinks(translation.rawText)

                        return {
                            rawText: translation.rawText,
                            convertedText: imageLinks
                        }
                    })
                setSigns(signArray)
            }
        ).catch(error => {
            clearLocalUser()
            somethingWentWrong(error)
            return history.push('/')
        }
        )
    }, [history])

    // Checks if user is logged in. Gets the user and sets all their translations to deleted:true
    // then updates the user in database
    const onDeleteAll = async () => {
        const loggedInUser = getLocalUser()

        const user = await fetchUser(loggedInUser).then(
            response => response
        ).catch(error => {
            clearLocalUser()
            somethingWentWrong(error)
            return history.push('/')
        })

        try {

            const deletedTranslations = user[0].translations.map((translation) => {
                const deletedTranslation = {
                    ...translation
                }
                deletedTranslation.deleted = true

                return deletedTranslation
            })
            const updatedUser = {
                ...user[0],
                translations: deletedTranslations
            }
            setSigns([])
            await putUser(updatedUser)

        } catch (error) {
            clearLocalUser()
            couldntUpdateUserAlert(error)
            return history.push('/')
        }
    }
    // Translate more button redirects to translate page
    const onTranslateMoreClick = () => {
        return history.push('/translate')
    }
    // Modal for delete all confirmation 
    // and if there are no translations to delete shows warning modal
    // made with react-confirm-alert
    const onTryDeleteAll = () => {
        if (signs.length === 0) {
            return confirmAlert({
                title: 'Warning',
                message: 'You have no translations to delete',
                buttons: [
                    {
                        label: 'Ok',
                        onClick: () => undefined
                    }
                ]
            })
        }
        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure you want to delete all translations?',
            buttons: [
                {
                    label: 'Delete everything',
                    onClick: () => onDeleteAll()
                },
                {
                    label: 'No',
                    onClick: () => undefined
                }
            ]
        })
    }

    return (
        <>
            <div key={shortid.generate()}>

                <h1 className='m-3'> {username}{
                    //Appending the apostrophe 's' when username doesn't end with 's'
                    username[username.length - 1] === 's' ? '' : '\'s'} saved translations </h1>
                <>
                    <div className='border border-2 border-secondary rounded' key={shortid.generate()}>
                        {
                            // slice and reverse to get latest first.
                            // using React Children in combination with generating a shortid to solve key missing error as no unique keys could be generated
                            Children.toArray(signs.slice(0).reverse().map((translation) => {
                                let images = translation.convertedText.map((imageUrl) => {
                                    //Hide all 'transparent' elements by setting opacity
                                    const opacity = imageUrl.includes('transparent') ? '0%' : '100%'
                                    return (<img style={{ width: '50px', opacity: opacity }} title={imageUrl.includes('transparent') ? 'space':imageUrl[imageUrl.length - 5]} src={imageUrl} key={shortid.generate()} alt='sign language character' />)
                                })
                                return (
                                    <>
                                        <h2 className='p-2' key={shortid.generate()}>{translation.rawText}</h2>
                                        <div key={shortid.generate()}>{images}</div>
                                        <hr key={shortid.generate()} className='m-0' />
                                    </>
                                )
                            }))
                        }
                    </div>
                </>
            </div>
            <div className='d-flex justify-content-between p-2'>
                <button className='btn btn-danger btn-lg' onClick={onTryDeleteAll}>
                    Delete all
                </button>
                <button className='btn' id="backtotranslate" onClick={onTranslateMoreClick} >
                    Back to translate
                </button>
            </div>
        </>
    )
}

export default Profile
