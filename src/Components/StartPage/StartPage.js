import "./Startpage.css"
import Login from "./Login"
import welcome from "../../assets/images/Logo-Hello.png"
import splash from "../../assets/images/Splash.svg"

// Component holding startpage images and login component
const StartPage = () => {
    return (
        <>
            <div className="imagescontainer">
                <img className="robotImage" src={welcome} alt='robot logo' />
                <img className="cloudImage" src={splash} alt='cloud background' />
            </div>
            <Login />
        </>
    )
}

export default StartPage
