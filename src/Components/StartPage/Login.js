import "./Login.css"
import { useState, useEffect } from "react"
import { useHistory } from "react-router-dom"
import { fetchUser, postUser } from "../../Util/Api"
import { setLocalUser, getLocalUser } from "../../Util/localStorage"
import { probablyJsonServerOfflineAlert } from "../../Util/alerts"

const Login = () => {
  const [username, setUsername] = useState('')
  const history = useHistory()

  useEffect(() => {
    // Fetches the logged in user if logged in redirects to profile
    function fetchUserAndRedirect() {
      const user = getLocalUser() ?? false
      if (user !== false) {
        history.push('/profile')
      }
    }
    fetchUserAndRedirect()
  }, [history])

  const onInputChange = (event) => {
    setUsername(event.target.value)
  }

  // Checks if username is empty. If user already exists logs them in else 
  // adds new user ot database
  // Redirects to translate page on new user login or old user login.
  // If anything went wrong shows probably json server alert and redirects to homepage  
  const onLogin = async (event) => {
    event.preventDefault()
    if (username === '') return alert("Username cannot be empty!")

    try {
      const user = await fetchUser(username.trim())
      if (user.length === 0) {
        await postUser(username.trim())

        const newUser = {
          username: username.trim(),
          loggedIn: true,
        }
        setLocalUser(newUser.username)
      }
      else {
        setLocalUser(user[0].username)
      }
    } catch (error) {
      return probablyJsonServerOfflineAlert(error.message)
    }
    return history.push("/translate")
  }

  return (
    <>
      <div className="formcontainer">
        <form id="form" className="mb-3 w-75 p-3 text-center login-form">
          <label htmlFor="username" className="form-label" hidden>
            Username
          </label>
          <input
            className="form-control mt-2"
            type="text"
            id="username"
            placeholder="Enter your username here..."
            onFocus={(e) => e.target.placeholder = ""}
            onBlur={(e) => e.target.placeholder = "Enter your username here..."}
            onChange={onInputChange}
          ></input>
          <button
            className="btn btn-primary btn-lg mt-3"
            type="submit"
            id="loginbutton"
            onClick={onLogin}
          >
            Login
          </button>
        </form>
      </div>
    </>
  )
}

export default Login
