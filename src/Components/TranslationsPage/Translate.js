import "./Translate.css"
import { fetchUser, putUser } from "../../Util/Api"
import { getLocalUser, loggedInCheck } from "../../Util/localStorage"
import signImageLinks from "../../Util/signImageLinks"
import { userNotLoggedInAlert, couldntSaveUserAlert } from "../../Util/alerts"
import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'

const Translate = () => {
  const [rawText, setrawText] = useState('')
  const [signs, setSigns] = useState([])
  const [username, setUsername] = useState('')

  const history = useHistory()

  // Checks if user is logged in, sets its value if it is or redirects to homepage.
  useEffect(() => {
    if (!loggedInCheck()) return history.push('/')
    
    const username = getLocalUser()
    setUsername(username)

  }, [history])

  // changes rawText whenever input changes
  const onInputChange = (event) => {
    event.preventDefault()
    setrawText(event.target.value)
  }

  // First checks if user is logged in, then uses regex to check if input is correct.
  // If all checks out creates a new Translation and puts its inside the user then updates the user in db.
  // imagelinks are generated to show the word
  const onSave = async (event) => {
    event.preventDefault()
    if (!loggedInCheck()) return history.push('/')
    
    const re = new RegExp(/[A-Z ]/gi)
    if (rawText.match(re) === null) return alert('Sorry can\'t translate that. Try again.')
    if (rawText.match(re).length !== rawText.length) return alert('String can only contain characters A-Z.')

    const rawTextPost = {
      rawText: rawText.trim(),
      deleted: false
    }

    let user = {}
    try {
      const users = await fetchUser(username)
      user = users[0]
    } catch (error) {
      userNotLoggedInAlert(error)
      return history.push('/')
    }

    user.translations = [...user.translations, rawTextPost]

    try {
      
      await putUser(user)
    } catch (error) {
      couldntSaveUserAlert(error)
      return history.push('/')
    }
    const imagesLinks = signImageLinks(rawText)
    setSigns(imagesLinks)

  }

  return (
    <>
      <form className='mb-3 form'>
        <label htmlFor='rawText' className='form-label'>Translate text into sign language</label>
        <input
          className='form-control mt-1' type='text' id="rawText"
          placeholder='Write here...'
          onFocus={(e) => e.target.placeholder = ''}
          onBlur={(e) => e.target.placeholder = 'Write here...'}
          onChange={onInputChange}
        ></input>

        <button
          className='btn btn-lg mt-3'
          id="translatebutton"
          type='submit'
          onClick={onSave}>Translate</button>
      </form>

      <section>
        <>{
          signs.length !== 0 &&
          <h3>Translation of {rawText}</h3>
        }
        </>
        <>{
          signs.length === 0 &&
          <p>Your translations will appear here...</p>
        }
        </>
        <p></p>
        <div id='translations'>
          <>
            {signs.map((link, index) => 
            <img src={link} title={link.includes('transparent') ? 'space':link[link.length - 5]} 
            style={{ width: '50px', opacity: (link.includes('transparent')) ? '0%' : '100%' }} 
            alt='sign language character' key={index} />)}
          </>
        </div>
      </section>
    </>

  )
}


export default Translate

