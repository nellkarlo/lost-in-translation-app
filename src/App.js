import "./App.css";
import 'bootstrap/dist/css/bootstrap.min.css'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from "./Components/Header/Header"
import StartPage from "./Components/StartPage/StartPage";
import NotFound from "./Components/NotFound/NotFound";
import Translate from "./Components/TranslationsPage/Translate";
import Profile from "./Components/ProfilePage/Profile";
import GuardedRoute from "./Components/Route/GuardedRoute";

function App() {
  return (
    <div className='container'>
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path='/' component={StartPage} />
          <GuardedRoute path='/translate' component={Translate}/>
          <GuardedRoute path='/profile' component={Profile}/>
          <Route path='*' component={NotFound} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
