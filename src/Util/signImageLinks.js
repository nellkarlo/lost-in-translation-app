// function that generates and array with appropriate sign language images based on the text that is passed in 
const signImageLinks = (text) =>{
    let imagesLinks = []

    for (let char = 0; char < text.length; char++) {
      const character = text[char].toLowerCase() === ' ' ? 'transparent' : text[char].toLowerCase()
      const imageLink = `individual_signs/${character}.png`
      imagesLinks.push(imageLink)

    }
    return imagesLinks
}
export default signImageLinks