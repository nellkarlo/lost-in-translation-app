const APP_NAME = 'LitApp'

// sets the localstorage user to the one username provided 
export const setLocalUser = (username) =>{
    return localStorage.setItem(APP_NAME, JSON.stringify(username))
}

// gets the localstorage user to the one username provided 
export const getLocalUser = () => {
    return JSON.parse(localStorage.getItem(APP_NAME))
}

// clears the localstorage user
export const clearLocalUser = () =>{
    localStorage.removeItem(APP_NAME)
}

// checks if user is logged in to localstorage 
// returns false if not and true if they are logged in. Else alerts user then returns false 
// which will history.push to startpage from where its called
export const loggedInCheck = () => {
    try {
        const loggedInUser = getLocalUser ?? null
        if (loggedInUser === null) {
            return false
        }
        return true
    } catch (error) {
        alert(`User is not logged in! Redirecting you to startpage... \n\n Error: ${error.message}`)
        return false
    }
}