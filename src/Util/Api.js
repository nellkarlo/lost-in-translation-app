// const JSON_SERVER_URL = 'http://localhost:3001' // saved for local debugging
const JSON_SERVER_URL = 'https://lit-fake-server.herokuapp.com'

// fetches user object from database
export const fetchUser = async (username) => {
  const response = await fetch(`${JSON_SERVER_URL}/users?username=${username}`)
  return await response.json()
}

// adds user object to database
export const postUser = async (username) => {
  const request = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      id: 0,
      username: username,
      translations: [],
    }),
  }

  return fetch(`${JSON_SERVER_URL}/users/`, request)
    .then((response) => response.json())
    .then((response) => response)
    .catch((error) => console.log(error.message))

}

// updates user object in database
export const putUser = async (user) => {
  const request = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(user),
  }
  await fetch(`${JSON_SERVER_URL}/users/${user.id}/`, request)
}