//broke out alerts to make the trycatches in the components look cleaner

export const userNotLoggedInAlert = (error) =>{
    return alert(`Error! You are not logged in. Redirecting to startpage!\n\n Error:${error.message}. `)
}

export const couldntSaveUserAlert = (error) =>{
    return alert(`Error! Couldn't save user. Redirecting to startpage!\n\n Error:${error.message}. `)
}

export const couldntUpdateUserAlert = (error) =>{
    return alert(`Error! Couldn't save user changes. Redirecting to startpage!\n\n Error:${error.message}. `)
}

export const probablyJsonServerOfflineAlert = (error) =>{
    return alert(`Error! Couldn't log you in. Attempt at creating new user or get existing user failed. 
    \n JSON Server is probably not running...

    Please try again 
    \n 
    Error:${error.message}. `)
}

export const somethingWentWrong = (error) =>{
    return alert(`Something went very wrong. Redirecting you to homepage! 
    Error:${error.message}. `)
}

